> ## ! Deprecated
>
> The content from the Quickstart is now included in the [Game System](https://gitlab.com/pacosgrove1/land-of-eem-rpg).

# Land of Eem RPG Quickstart

![alt text](assets/setup.webp)

A FoundryVTT implementation of the [Land of Eem RPG Quickstart](https://preview.drivethrurpg.com/en/product/322009/land-of-eem-rpg-quickstart-guide).

## Installation

Copy the URL into the Manifest URL field of the Install Module dialog:

`https://gitlab.com/api/v4/projects/pacosgrove1%2Fland-of-eem-quickstart-guide/packages/generic/land-of-eem-quickstart-guide/latest/module.json`
